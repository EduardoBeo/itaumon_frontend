
function validaLogin(){

    let userTxt = localStorage.getItem("userLogged");

    if(!userTxt){
        window.location = "index.html";
    }

    let jsonUser = JSON.parse(userTxt);
    if(jsonUser != null){
        loginAprovado(jsonUser);
    }else{
        window.location = "index.html";
    }
}

function loginAprovado(user){
    document.getElementById("racf").innerHTML = `${user.racf} ` ;
    document.getElementById("nome").innerHTML = `${user.nome} ` ;
    document.getElementById("imgUser").innerHTML = `<img src ="${user.linkFoto}">`;
    
    fetch("http://localhost:8080/agencias")
    .then(res => res.json())
    .then(result => preencheAgencias(result));

}

function preencheAgencias(resposta){
    let agencias = '';
    for (let index = 0; index < resposta.length; index++) {
        agencias = agencias + `<option value = ${resposta[index].idAgencia}> ${resposta[index].nomeAgencia} </option>`;
    }
    document.getElementById("sel_agencias").innerHTML = agencias;
}

function consultar(evento){

    evento.preventDefault();

    let campoAgencia = document.getElementById("sel_agencias");
    let idAgencia = campoAgencia[campoAgencia.selectedIndex].value; 

    fetch("http://localhost:8080/feriado/agencia/" + idAgencia)
    .then(res => res.json())
    .then(result => montarTabela(result));

}

function montarTabela(resposta) {
    let tabelaFeriados = '<table class = "table"> <tr> <th>Inicio/Fim</th> <th>Feriado</th> <th>Agência</th> </tr>';

    for (let index = 0; index < resposta.length; index++) {
        tabelaFeriados = tabelaFeriados + `<tr> 
            <td> ${resposta[index].dataInicio} à ${resposta[index].dataFim}</td> 
            <td> ${resposta[index].nomeFeriado} </td>
            <td> ${resposta[index].agencia.nomeAgencia} </td>
        </tr>`;
    }

    tabelaFeriados = tabelaFeriados + '</table>';

    document.getElementById("tabela").innerHTML = tabelaFeriados;
}

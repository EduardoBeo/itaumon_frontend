
function validaLogin(){

    let userTxt = localStorage.getItem("userLogged");

    if(!userTxt){
        window.location = "index.html";
    }

    let jsonUser = JSON.parse(userTxt);
    if(jsonUser != null){
        loginAprovado(jsonUser);
    }else{
        window.location = "index.html";
    }
}

function loginAprovado(user){
    console.log(user);
    document.getElementById("racf").innerHTML = `${user.racf} ` ;
    document.getElementById("nome").innerHTML = `${user.nome} ` ;
    document.getElementById("imgUser").innerHTML = `<img src ="${user.linkFoto}">`;
    // document.getElementById("idLogado").value = user.id;
    
    fetch("http://localhost:8080/agencias")
    .then(res => res.json())
    .then(result => preencheAgencias(result));

}

function preencheAgencias(resposta){
    let agencias = '';
    for (let index = 0; index < resposta.length; index++) {
        agencias = agencias + `<option value = ${resposta[index].idAgencia}> ${resposta[index].nomeAgencia} </option>`;
    }
    document.getElementById("sel_agencias").innerHTML = agencias;
}

function cadastrarFeriado(evento){

    evento.preventDefault();

    let campoAgencia = document.getElementById("sel_agencias");
    let idAgencia = campoAgencia[campoAgencia.selectedIndex].value; 

    let novoFeriado = {
        //id: null,
        agencia:{idAgencia:idAgencia},
        nomeFeriado:document.getElementById("txtFeriado").value,
        dataInicio: document.getElementById("txtInicio").value,
        dataFim: document.getElementById("txtFim").value
    }

    let cabecalho = {
        method: 'POST',
        body: JSON.stringify(novoFeriado),
        headers:{
            'Content-type':'application/json'
        }
    }
    console.log(cabecalho)
    fetch("http://localhost:8080/feriado/novo", cabecalho)
    .then(res => res.json())
    .then(result => console.log(result));

    //window.location = "consulta.html";
}


